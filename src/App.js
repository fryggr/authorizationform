import React, {Component} from 'react';
import './App.css';
import {Input} from "./Components/Input/Input";
import {Button} from "./Components/Button/Button";

class App extends Component {
    constructor(props){
        super(props);

        this.state = {
            login: '',
            password: ''
        }

        this.login = /([^A-Za-z0-9])/g;
        this.password = /([^A-Za-z0-9!@#$%^&*()])/g;
    }

    handleInput(e){
        let str = e.target.value;
        this.setState({[e.target.name]: str});
        if(this[e.target.name].test(str)){
            // str = str.replace(str.replace(this.regLogin, ''), '');
            str = str.replace(this[e.target.name], '');
            this.setState({[e.target.name]: str});
        }
    }

    validate(){
        alert(`Привет, ${this.state.login}`)
    }

    render() {
        return (
            <div className="App">
                <form className="Login" onSubmit={this.validate.bind(this)}>
                    <Input
                        placeholder="Логин"
                        pattern="[A-Z][a-z|0-9]{2,}"
                        defaultValue="Aa1"
                        value={this.state.login}
                        validate={this.handleInput.bind(this)}
                        name="login"
                    />
                    <Input
                        placeholder="Пароль"
                        pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z])((?=.*!)|(?=.*@)|(?=.*#)|(?=.*\$)|(?=.*%)|(?=.*\^)|(?=.*&)|(?=.*\*)|(?=.*\()|(?=.*\))).{8,}"
                        defaultValue="Aa1"
                        value={this.state.password}
                        validate={this.handleInput.bind(this)}
                        name="password"
                    />
                    <Button
                        text="Войти"
                        type="submit"
                    />
                </form>
            </div>
        );
    }
}

export default App;
