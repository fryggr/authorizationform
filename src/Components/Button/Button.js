import React from "react";
import "./Button.css"

export const Button = (props) => {
    return(
        <button className="Button" type={props.type} >{props.text}</button>
    )
}
