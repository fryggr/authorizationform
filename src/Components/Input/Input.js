import React from "react";
import "./Input.css"

export class Input extends React.Component {
    constructor(props) {
        super(props)
    }

    render(){
        console.log(this.props.value);
        return(
            <input
                required
                type="text"
                placeholder={this.props.placeholder}
                className="Input"
                pattern={this.props.pattern}
                onChange={this.props.validate}
                value={this.props.value}
                name={this.props.name}
            />
        )
    }
}
